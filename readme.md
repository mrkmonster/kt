Kalte Torte
===========

Ingredients
-----------

- 2 packets of *Bahlsen Leibniz-Keks* biscuits (or Morning Coffee biscuits if unavailable)
- 3 tablespoons of icing sugar
- 3 tablespoons of cocoa powder
- 3 tablespoons of milk
- 1 egg
- 125g of *Dr Schlinck's Palmin Kokosfett* (or coconut fat if unavailable)

Method
------

1. Whisk egg and mix well with sugar, cocoa powder, and milk.
2. Beat coconut fat until melted and mix together with rest of ingredients, stirring well. *Vanilla essence, cognac, or rum may be added if required.*
3. Line a square loaf tin with greaseproof paper then build with biscuits, mix, biscuits, mix, etc.
4. Refrigerate for 1 hour.
